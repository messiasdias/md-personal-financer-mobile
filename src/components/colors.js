import {StyleSheet } from 'react-native'

const colors =  {
    white:'#FFFFF',
    black:'#000',
    light: '#ccc', 
    light2: '#999', 
    blue: '#0055cc', //mokups
    blue2:'#4169e1' ,//royalblue 
    blue3: '#87ceeb', //skyblue 
    blue4: '#6a5acd', //slateblue 
    success: '#3cb371', //mediumspringgreen
    danger: '#ff6347', //tomato
    warning: '#f0e68c' //khaki
}


//export default StyleSheet.create(colors)
export default colors