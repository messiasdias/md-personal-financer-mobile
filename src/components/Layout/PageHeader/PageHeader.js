import React, { Component } from 'react'
import {TouchableOpacity,View, Text} from 'react-native'
import styles from './styles'
import Icon from 'react-native-vector-icons/FontAwesome'
import { DrawerActions } from 'react-navigation-drawer'



export default  class PageHeader extends Component {

  constructor(props){
    super(props)
  }


  render() {

    return (
      
        <View style={styles.page_header}>

          <View style={styles.left} >
            <Text style={styles.left_text} >{ this.props.title ? this.props.title : '...' }</Text>
            <Icon style={styles.left_icon} name="chevron-down" />
          </View>

          <TouchableOpacity onPress={ () => this.props.navigation ?  this.props.navigation.dispatch(DrawerActions.toggleDrawer()) : alert("No Navegation!") } >
            <Icon style={styles.menu_icon} name={ this.props.title && ( this.props.title !== "Menu" ) ? 'bars' : 'times'} />
          </TouchableOpacity>

        </View>
      );
   }



}

