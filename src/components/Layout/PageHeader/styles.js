import {StyleSheet } from 'react-native'
import colors from './../../colors'

const styles = StyleSheet.create({
    page_header: {
        borderBottomWidth: 3,
        borderColor: '#ccc',
        width:'90%',
        minHeight:60,
        display: 'flex',
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        alignSelf:"center"
    },

    left:{
        padding: 10,
        display: 'flex',
        flexDirection:'row',
        alignItems:'center',
        justifyContent: 'center'
    },
    
    left_text:{
        color: colors.light,
        marginRight: 10,
        fontSize: 20,
        fontWeight: "bold",

    },

    left_icon:{
        fontSize:16,
        color: colors.light
    },

    menu_icon:{
        fontSize: 40,
        color: colors.light,
        marginRight: 10
    },

  })
  
  export default styles