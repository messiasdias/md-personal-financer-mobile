import AppStyle from './../../../tools/style/AppStyle'

const styles = {


   menu_list:{
        color: '#ccc',
        padding: 20,
        margin: 20,
        maxWidth:"60%",
    },

    menu_list_item:{
        color: '#999',
        fontSize: 20,
        padding:5,
        paddingTop:10,
        width:"100%",
    }, 

    pr_btn: {
        marginTop: 30,
        marginBottom: 30, 
        alignSelf: 'center',

    },
    
    logout:{
        width: 100, 
        alignSelf:'flex-end' , 
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: "center",
        padding: 20,
        marginBottom: -20,
        marginRight: 0,
    },

    logout_text:{
        marginRight: 5, 
        fontSize: 20,
    },



}
  



export default AppStyle.create(styles)