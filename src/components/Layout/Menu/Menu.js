import React, { Component } from 'react'
import {View,Text,SafeAreaView, BackHandler} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import styles from './styles'
import PageHeader from './../PageHeader/PageHeader'
import Btn from './../Btn/Btn'
import MenuList from './MenuList' 
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'


export default class Menu extends Component {

    constructor(props){
        super(props)
    }

    navigate(route){
        return () => { this.props.navigation.navigate(route) }
    }

    render() {


        const ListData = [
            {
                text: 'Início',
                route: 'home'
            },
            {
                text: 'Nova Receita',
                route: 'new_receita'
            },
            {
                text: 'Nova Despesa',
                route: 'new_despesa'
            },
            {
                text: 'Relatórios Salvos',
                route: 'relatorios'
            },
        ]

        return (
        <View style={[styles.app]}>
            <PageHeader navigation={this.props.navigation}   title="Menu" />

            <SafeAreaView style={styles.menu_list} >
                    <MenuList  styles={styles} navigation={this.props.navigation} data={ListData} />
            </SafeAreaView>
           
            <ScrollView style={[styles.scroll]}  >
                
                <View style={[ styles.hr2 ]} />

                <Btn icon="search" text="Buscar" btnstyle={ [styles.bk_blue2,  styles.pr_btn ] } route="buscar" navigation={this.props.navigation} />   

             </ScrollView>

             <TouchableOpacity style={ [ styles.logout ]} onPress={ () => {BackHandler.exitApp()} }  >
                    <>
                        <Text style={[styles.logout_text, styles.light2 ]} >Sair</Text>
                        <Icon name="sign-out" style={[styles.logout_text, styles.light2 ]}  />
                    </>
            </TouchableOpacity>

        </View> 
        )
    }
}

