import React, { Component } from "react";
import {Text, FlatList } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import styles from './styles'


export default class MenuList extends Component{


    constructor(props){
        super(props)
    }

    render(){

         const ListData = [
            {
                text: 'Hoje',
                route: 'home'
            },
            {
                text: 'Últimos 3 Dias',
                route: '3dias'
            },
            {
                text: 'Últimos 3 Meses',
                route: '3meses'
            },
            {
                text: 'Últimos 3 Anos',
                route: '3anos'
            },
            {
                text: 'Relatórios Salvos',
                route: 'relatorios'
            },
        ]
    

        return(
        
                <FlatList 
                    data={this.props.data ? this.props.data : ListData}
                    navigation={this.props.navigation} 
                    renderItem={ ({item}) => {
                        return (
                            <TouchableOpacity onPress={ () => { this.props.navigation.navigate(item.route) } } >
                                <Text style={styles.menu_list_item  }>{ item.text }</Text> 
                            </TouchableOpacity>
                        )
                    }  }
                    keyExtractor={item => item.route }
                />  
            
        )

    }

}