import React, { Component } from "react"
import {Text} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/FontAwesome'
import styles from './styles'

export default class Btn extends Component{

    constructor(props){
        super(props)
    }

    render(){

        return(

            <TouchableOpacity  onPress={ () => { ((this.props && this.props.navigation ) && this.props.route ) ?  this.props.navigation.navigate(this.props.route, this.props.params ? this.props.params : {} ) : false } } style={[ styles.btn, this.props.btnstyle ? this.props.btnstyle : styles.bk_light ,  styles.btn_icon ]} >
                <>
                    <Icon style={styles.btn_icon }  name={this.props.icon ? this.props.icon : '' }  />
                    <Text style={styles.btn_text} >{this.props.text ? this.props.text : "No Text" }</Text>
                </>
            </TouchableOpacity>

        )


    }

}