import React, {Component} from 'react'
import PureChart from 'react-native-pure-chart'
 
export default class HomeGraphic extends Component {
 
    constructor(props){
        super(props)
    }

    render() {

        let sampleData = [
            {
              seriesName: 'Bolso',
              data: [
                {x: 'anteontem', y: 2},
                {x: 'ontem', y: 10},
                {x: 'hoje', y: 1},
                
              ],
              color: 'green'
            },
            {
              seriesName: 'Despesas',
              data: [
                {x: 'anteontem', y: 10},
                {x: 'ontem', y: 80},
                {x: 'hoje', y: 66},
              ],
              color: 'tomato'
            },

            {
                seriesName: 'Receitas',
                data: [
                  {x: 'anteontem', y: 10 },
                  {x: 'ontem', y: 400},
                  {x: 'hoje', y: 78},
                ],
                color: 'royalblue'
              }
          ]
        return (
            <PureChart width={this.props.width} height={this.props.height} data={sampleData}  type={this.props.type} />
        )
    }
 
}
 