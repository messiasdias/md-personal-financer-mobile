import AppStyle from './../../../tools/style/AppStyle'

const styles = {

    item: { 
        flexDirection: "row",
        alignItems: "center" , 
        justifyContent: "flex-start", 
        padding: 5,  
     },

    description:{
        fontSize: 20,
        minWidth: '22%',
        textAlign: "left",
    },

    description2:{
        fontSize: 30,
        minWidth: '55%',
        maxWidth: '55%',
       textAlign: "right",
       position: "absolute",
       right: '6%',
    },

    description3:{
        width: '5%',
        fontSize: 12,
        margin: 5,
        position: "absolute",
        right: -10,
        top: 0,
    },

    description_icon:{
        fontSize: 20,
        marginRight: 10,
    },

    list:{
        color: '#ccc',
        padding: 20,
        display:"flex",
        flexDirection: "column",
        justifyContent: "space-around",
        alignItems: "flex-start",
        flex:2,
        display: "none",
       
    },

    list_item:{
        color: '#999',
        fontSize: 20,
        padding: 10,
        textAlign: "left",
    },

    graphic:{
        minWidth: "60%",
        width: "100%",
        height: 100,
        minHeight: "100%",
        alignSelf:"center",
        flex:2
    },


    list_graphic_content:{
        display:'flex',
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-around",
        flex: 1,
    },


    
  }
  
  

  export default AppStyle.create(styles)