import React, { Component } from 'react'
import {ScrollView, FlatList, View, Text, SafeAreaView} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/FontAwesome'
import PageHeader from '../../Layout/PageHeader/PageHeader'
import styles from './styles'
import Btn from './../../Layout/Btn/Btn'
import MenuList from './../../Layout/Menu/MenuList' 
import HomeGraphic from './HomeGraphic'
import PureChart from 'react-native-pure-chart';


export default class Home extends Component {

  constructor(props){
    super(props)
  }

  render() {

    let sampleData = [
      {
        value: 50,
        label: 'Marketing',
        color: 'red',
      }, {
        value: 40,
        label: 'Sales',
        color: 'blue'
      }, {
        value: 25,
        label: 'Support',
        color: 'green'
      }
   
    ]

    return (
      <View style={[styles.app, {paddingBottom: 40}]} >
      
          <PageHeader navigation={this.props.navigation} title={this.props.navigation.state.params.title} / >
    
          <ScrollView style={[styles.scroll]}  > 

        

          <View style={[styles.content  ] }>
              <View style= {[ styles.item ] } >
                <Icon  name="credit-card" style={[styles.success, styles.description_icon ] } />
                <Text style={[styles.success, styles.description] }>Carteira</Text>
                <Text style={[styles.success, styles.description2] }>1598,76</Text>
                <Text style={[styles.success, styles.description3] }>R$</Text>
              </View>

              <View style={styles.hr2} />

              <View style={[ styles.item  ]} >
                <Icon  name="remove" style={[styles.danger, styles.description_icon ] } />
                <Text style={[styles.description, styles.danger]}>Despesas</Text>
                <Text style={[styles.description2, styles.danger]}>199,90</Text>
                <Text style={[styles.description3, styles.danger]}>R$</Text>
              </View>

              <View style={styles.hr2} />


            <View style={[styles.item  ]}>
              <Icon  name="plus" style={[styles.blue2, styles.description_icon ] } />
              <Text style={[styles.description, styles.blue2]}>Receitas</Text>
              <Text style={[styles.description2, styles.blue2 ]}>80,00</Text>
              <Text style={[styles.description3, styles.blue2 ]}>R$</Text>
            </View>

           </View> 
        

            <View style={[styles.content, styles.list_graphic_content  ] }>

               <Text>Útimos 3 Dias</Text>
                <HomeGraphic 
                    width={styles.graphic.width } 
                    height={styles.graphic.height}
                    styles={styles.graphic}
                    type={'bar'}
                    labels={[]}  
                    gdata={[]}
                />
                </View>


                <View style={[styles.content, styles.list_graphic_content, {flexDirection: 'row-reverse'}  ] }>


                <Text>Útimos 3 Meses</Text>
                <HomeGraphic 
                    width={styles.graphic.width } 
                    height={styles.graphic.height}
                    styles={styles.graphic}
                    type={'line'}
                    labels={[]}  
                    gdata={[]}
                />

                </View>


                <View style={[styles.content, styles.list_graphic_content ] }>

                    <Text>Útimos 3 Anos</Text>

                    <PureChart 
                        width={styles.graphic.width } 
                        height={styles.graphic.height}
                        type={'pie'}
                        data={ sampleData}
                     />

                  </View>




                <View style={[styles.content, styles.list_graphic_content  ] }>
                <View style={styles.list}  >
                
                    <TouchableOpacity onPress={ () => { this.props.navigation.navigate('3meses') } } >
                        <Text style={styles.list_item  }>Últimos 3 Meses</Text> 
                    </TouchableOpacity>
                    
                    <TouchableOpacity onPress={ () => { this.props.navigation.navigate('3anos') } } >
                      <Text style={styles.list_item  }>Últimos 3 Anos</Text> 
                    </TouchableOpacity>
                    
                    <TouchableOpacity onPress={ () => { this.props.navigation.navigate('relatorios') } } >
                        <Text style={styles.list_item  }>Relatórios Salvos</Text> 
                    </TouchableOpacity>
                </View>
            
            </View>
         
          

            <View style={[styles.btn_content  ]} >
              <Btn 
                icon="plus" 
                text="Receita" 
                btnstyle={styles.bk_blue2} 
                route="new_credito" 
                navigation={this.props.navigation} 
              />
              
              <Btn 
                icon="plus" 
                text="Despesa"  
                btnstyle={styles.bk_danger} 
                route="new_despesa" 
                navigation={this.props.navigation} 
              />

            </View>   
          
        

        </ScrollView> 
      
      </View>

    )
    
  }


}

