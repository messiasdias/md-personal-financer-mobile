import AppStyle from './../../../tools/style/AppStyle'


const styles = {
  
    primary:{
        ...AppStyle.styles.btn,
        ...AppStyle.styles.bk_blue2,
        ...AppStyle.styles.white
    }
  
}


export default  AppStyle.create( styles  )  