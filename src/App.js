import React, {Component} from 'react'
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer'
import 'react-native-gesture-handler'
import Routes from './tools/Routes'

const Router = createDrawerNavigator(Routes[0], Routes[1])
const AppContainer =  createAppContainer(Router)

 export default class App extends Component {
  render(){
    return (
      <AppContainer/>
    )
  }
}


