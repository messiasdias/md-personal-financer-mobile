/* 
*   App default Styles
*
*/


var style = {

    teste: {
        borderWidth: 1,
        borderColor: 'red',
        borderStyle: 'solid'
    },

    //Start global StyleSheet
    app: {
        width:'100%',
        height: '100%' ,
        paddingTop: 10,
        marginBottom: 80,
        paddingBottom: 30,
        backgroundColor: '#FFFFFF',
    },

    scroll:{
        width: "100%",
        padding: 20,
    },

    content: {
        padding: 20,
        marginTop: 10,
        marginBottom: 10,
        display:'flex',
        flexDirection: 'column',
        width:'100%', 
        alignSelf: "center" ,
        backgroundColor: '#FFFFFF',
    },
    
    content_item:{
        display: 'flex',
        flexDirection: 'row' ,
        alignItems: 'center',
        padding: 5,
    },


    //Background Colors

      bk_white:{
            backgroundColor: '#FFFFFF'
      },
      bk_black:{
            backgroundColor: '#000'
      },
  
      bk_light:{
          backgroundColor: '#ccc'
      },
  
      bk_light2:{
          backgroundColor: '#999'
      },
  
      bk_blue:{
          backgroundColor: '#0055cc'
      },
  
      bk_blue2:{
          backgroundColor: 'royalblue'
      },
  
      bk_blue3:{
          backgroundColor: 'skyblue'
      },
      bk_blue4:{
          backgroundColor: 'slateblue'
      },
  
      bk_success:{
          backgroundColor: '#90EE90', //mediumspringgreen
      },
  
      bk_danger:{
          backgroundColor: 'tomato'
      },
  
      bk_warning:{
          backgroundColor: 'khaki'
      },
  
      //Font Colors
  
      white:{
          color: '#FFFFFF'
      },
      black:{
          color: '#000'
      },
  
      light:{
          color: '#ccc'
      },
      light2:{
          color: '#999'
      },
  
      blue:{
          //mokups
         color: '#0055cc'
      },
  
      blue2:{
         color: 'royalblue'
      },
  
      blue3:{
         color: 'skyblue'
      },
      blue4:{
         color: 'slateblue'
      },
  
      danger:{
          color: 'tomato'
      },
  
      success:{
          color:  '#008000', //mediumspringgreen 90EE90
      },
        
      warning:{
          color: 'khaki'
      },


    //btn
    btn:{
        maxWidth: '80%',
        minWidth: 250,
        padding: 20,
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: "center",
        borderRadius: 75,
        maxHeight: 50,
        minHeight: 50,
        backgroundColor: '#ccc',
        flex:1,
        flexDirection: 'row',
    },

    btn_icon: {
        fontSize: 16,
        marginRight: 10,
        color: 'white',
    },

    btn_text: {
        fontSize: 18,
        color: 'white',
        fontWeight: '100',
        letterSpacing: 1
    },

    btn_content:{
        display: 'flex',
        flexDirection: 'column',
        flexWrap: "wrap",
        alignItems: 'center',
        justifyContent: 'center',
        padding: 30,
        alignSelf: "center",
    },
   

    //hr
    hr: {
        borderBottomColor: '#ccc',
        borderBottomWidth: 2,
        borderStyle:'dotted',
        width:'90%',
        alignSelf: "center",
    },

    hr2:{ 
        borderStyle: "dotted", 
        borderBottomColor: '#999', 
        borderBottomWidth: .86,
    },

    //Fix
    fix_on_top : {
        position: 'absolute', 
        top: 0,
        flex:1
    },
   
    fix_on_bottom : {
        position: 'absolute', 
        bottom: 0,
        flex: 1
    }
   


} 

export default style







