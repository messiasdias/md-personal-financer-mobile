
import {StyleSheet} from 'react-native'
import style_default from './dafault'
//import style_extends from './extends'

class AppStyle {

    constructor(style_default){
       this.styles = style_default
       //this.extends = style_extends
    }  
    
  static styles
  //static extends

  /* Push/Merge two StyleSheet Objects
     Merge two or more stylesheets
     use --> app_style.merge( styles1, styles2, styles3 ) //styles*/
  push ( ...args ) { 
      return Object.assign( this.styles, this.extends, ...args ) 
   }


  //merge or extens two or more class styles in one class
  //return style_extended/merged use --> app_style.merge( styles1, styles2, styles3 )
  merge( ...args ) {
       return  Object.assign( ...args) 
  }
  
  
  //create  StyleSheet.create(style)
  create( ...args ) {
       return  StyleSheet.create( this.push(...args) ) 
   }


} 



export default new AppStyle(style_default)