import {Animated, Easing} from 'react-native'
import Menu from './../components/Layout/Menu/Menu'
import Home from './../components/Pages/Home/Home'
import Periodo from './../components/Pages/Periodo/Periodo'


const  TransitionConfig = () => ({
  transitionSpec: {
    duration: 5,
    timing: Animated.timing,
    easing: Easing.step0
  }
})

const Routes = [
   {
    //Pages/Screens
    'home': { //Hoje - Home
            screen: Home,
            params: { title: 'Início' },
    },

    '3dias': { //Ultimos 3 dias
        screen: Periodo,
        params: { title: 'Últimos 3 Dias' },
    },

    '3meses': {//Ultimos 3 meses
        screen: Periodo,
        params: { title: 'Últimos 3 Meses' },
    },

    '3anos': { //Ultimos 3 anos
        screen: Periodo,
        params: { title: 'Últimos 3 Anos' },
     },

     'buscar':
     { //Buscar periodo
        screen: Periodo,
        params: { title: 'Buscar por Período' },
     },

     'relatorios': 
     { //Relatórios salvos
        screen: Periodo,
        params: { title: 'Relatórios Salvos' },
     },

     new_receita: { //Buscar Novo Crédito
      screen: Periodo,
      params: { title: 'Nova Receita' },
     },

     new_despesa:{ //Buscar Nova Despesa
      screen: Periodo,
      params: { title: ' Nova Despesa' }
     }

     //end Routes and start config
    },
    {
      initialRouteName: 'home', // home
      drawerType: 'front',
      drawerPosition: 'left',
      drawerWidth: '100%',
      drawerBackgroundColor: '#ccc',
      contentComponent: Menu,
      transitionConfig : TransitionConfig,
    }

]

  export default Routes